﻿//Server.cpp
#pragma comment(lib,"ws2_32.lib")
#include <ws2tcpip.h>
#include <WinSock2.h>
#include <iostream>

const int MaximumPlayers = 2;														// maximum amount of player that can take control over a player character

SOCKET Connections[MaximumPlayers - 1];												// maximum amount of connections (remember that it starts at 0).
int ConnectionCounter = 0;															// what position the connection have.

bool debugAll = false;																// only for debuging 

bool serverRunning = false;															// if server is online and the main loop is still running ( set to true at start of main)

bool awaitReconnect = false;														// if we have lost a connection and wait for a new player to connect
bool p1 = false;																	// player 1 is active
bool p2 = false;																	// player 1 is active

float ServerAcceptablePlayerSpeed = 0.06;											// the maximum speed player can move with before server will correct server (note that player if player speed is 0.05 then server must allow 0.06 else ther will be inacurate corrections)

// -- gamestate values -- //

//Player positions
float p1X = 8;																		// server sidedd player position
float p1Y = 2;																		// server sidedd player position
float p1Z = -2;																		// server sidedd player position

float p2X = 24;																		// server sidedd player position
float p2Y = 2;																		// server sidedd player position
float p2Z = -2;																		// server sidedd player position

// bools
bool p1Alive = true;																// player bools
bool p1Goal = false;																// player bools

bool p2Alive = true;																// player bools
bool p2goal = false;																// player bools

// -- end gamestate values -- //


enum Packet		// create a enum to different packets
{
	P_Unknown, P_Connect, P_PlayerInput, P_GameState,
};

bool SendPacketID(int index, Packet _packetid)
{
	int ReturnCheck = send(Connections[index], (char*)&_packetid, sizeof(Packet), NULL);								// send packet so the reciver know how to collect the information	
	if (ReturnCheck == SOCKET_ERROR)
		return false;
	return true;
}

bool GetPacketID(int index, Packet &_packetid)
{
	int ReturnCheck = recv(Connections[index], (char*)&_packetid, sizeof(Packet), NULL);								// send packet so the reciver know how to collect the information	
	if (ReturnCheck == SOCKET_ERROR)
		return false;
	return true;
}

bool ServerProcessIncomingPacket(int index, Packet packetid)
{
	Packet packetID = packetid;
	switch (packetid)
	{
	case P_Connect:
	{
	}
	case P_PlayerInput:
	{
		int arrowDirection;
		int playerId;	
		bool sendReady = true;
		recv(Connections[index], (char*)&playerId, sizeof(int), NULL);			// recv player id
		recv(Connections[index], (char*)&arrowDirection, sizeof(int), NULL);	// recv arrow direction from player
		//std::cout << "Client// p" << playerId << " sent packet " << packetID << " with the ArrowKey: " << arrowDirection << "." << std::endl; // inform server what was recived
		Packet sendNewPacketID = P_PlayerInput;									// this packet will comfirm the input from the player and sent it to all players
		for (int i = 0; i < MaximumPlayers; i++)								// loop throu all connections
		{
			send(Connections[i], (char*)&sendNewPacketID, sizeof(Packet), NULL);//send back the packet so client can recv
			send(Connections[i], (char*)&sendReady, sizeof(sendReady), NULL);
			send(Connections[i], (char*)&playerId, sizeof(int), NULL);			//send back the pplayer id of the key press
			send(Connections[i], (char*)&arrowDirection, sizeof(int), NULL);	//send back the result to player
		}
		//Packet sendNewGamestatePacketID = P_GameState;							// this packet will make corrections to the client
		//for (int i = 0; i < MaximumPlayers; i++)								// loop throu all connections
		//{
		//	send(Connections[i], (char*)&sendNewPacketID, sizeof(Packet), NULL);
		//	send(Connections[i], (char*)&playerId, sizeof(int), NULL);
		//}
		break;
	}
	case P_GameState:
	{
		int playerId;
		float pPosX;
		float pPosY;
		float pPosZ;
		recv(Connections[index], (char*)&playerId, sizeof(int), NULL);			// recv player id
		recv(Connections[index], (char*)&pPosX, sizeof(pPosX), NULL);			// recv position data from player
		recv(Connections[index], (char*)&pPosY, sizeof(pPosY), NULL);			// recv position data from player
		recv(Connections[index], (char*)&pPosZ, sizeof(pPosZ), NULL);			// recv position data from player
		if(debugAll == true)
		std::cout << std::endl << "player new pos: " << pPosX << " " << pPosY << " " << pPosX << ".!" << std::endl;
		Packet sendNewPacketID = P_GameState;									// this packet will comfirm the input from the player and sent it to all players
		for (int i = 0; i < MaximumPlayers; i++)								// loop throu all connections
		{
			send(Connections[i], (char*)&sendNewPacketID, sizeof(Packet), NULL);//send back the packet so client can recv
			send(Connections[i], (char*)&playerId, sizeof(int), NULL);			//send back the pplayer id of the key press
			send(Connections[i], (char*)&pPosX, sizeof(pPosX), NULL);			//send back the result to player
			send(Connections[i], (char*)&pPosY, sizeof(pPosY), NULL);			//send back the result to player
			send(Connections[i], (char*)&pPosZ, sizeof(pPosZ), NULL);			//send back the result to player
			// below is the code that let the server dictate how the will move in game
			if (playerId == 1)
			{
				if (pPosX >(p1X + ServerAcceptablePlayerSpeed) || pPosX < (p1X - ServerAcceptablePlayerSpeed) || pPosY >(p1Y + ServerAcceptablePlayerSpeed) || pPosY < (p1Y - ServerAcceptablePlayerSpeed) || pPosZ >(p1Z + ServerAcceptablePlayerSpeed) || pPosZ < (p1Z - ServerAcceptablePlayerSpeed))
				{
					//std::cout << "test fixing p1 position" << std::endl;
					if (pPosX >(p1X + ServerAcceptablePlayerSpeed))
						pPosX = p1X + 0.05;
					if (pPosX < (p1X - ServerAcceptablePlayerSpeed))
						pPosX = p1X - 0.05;
					if (pPosY >(p1Y + ServerAcceptablePlayerSpeed))
						pPosY = p1Y + 0.05;
					if (pPosY < (p1Y - ServerAcceptablePlayerSpeed))
						pPosY = p1Y - 0.05;
					if (pPosZ >(p1Z + ServerAcceptablePlayerSpeed))
						pPosZ = p1Z + 0.05;
					if (pPosZ < (p1Z - ServerAcceptablePlayerSpeed))
						pPosZ = p1Z - 0.05;
				}
			}
			else if (playerId == 2)
			{
				if (pPosX >(p2X + ServerAcceptablePlayerSpeed) || pPosX < (p2X - ServerAcceptablePlayerSpeed) || pPosY >(p2Y + ServerAcceptablePlayerSpeed) || pPosY < (p2Y - ServerAcceptablePlayerSpeed) || pPosZ >(p2Z + ServerAcceptablePlayerSpeed) || pPosZ < (p2Z - ServerAcceptablePlayerSpeed))
				{
					if (pPosX >(p2X + ServerAcceptablePlayerSpeed))
						pPosX = p2X + 0.05;
					if (pPosX < (p2X - ServerAcceptablePlayerSpeed))
						pPosX = p2X - 0.05;
					if (pPosY >(p2Y + ServerAcceptablePlayerSpeed))
						pPosY = p2Y + 0.05;
					if (pPosY < (p2Y - ServerAcceptablePlayerSpeed))
						pPosY = p2Y - 0.05;
					if (pPosZ >(p2Z + ServerAcceptablePlayerSpeed))
						pPosZ = p2Z + 0.05;
					if (pPosZ < (p2Z - ServerAcceptablePlayerSpeed))
						pPosZ = p2Z - 0.05;
				}
			}
			if (playerId == 2)													// if the playerid is 2 then we want to send player 2 last pos
			{
				p2X = pPosX;													// update the servers own values
				p2Y = pPosY;													// update the servers own values
				p2Z = pPosZ;													// update the servers own values
				send(Connections[i], (char*)&p2X, sizeof(p2X), NULL);			//send back the result to player
				send(Connections[i], (char*)&p2Y, sizeof(p2Y), NULL);			//send back the result to player
				send(Connections[i], (char*)&p2Z, sizeof(p2Z), NULL);			//send back the result to player
			}
			else if (playerId == 1)												// if the playerid is 1 then we want to send player 1 last pos
			{
				p1X = pPosX;													// update the servers own values
				p1Y = pPosY;													// update the servers own values
				p1Z = pPosZ;													// update the servers own values
				send(Connections[i], (char*)&p1X, sizeof(p1X), NULL);			//send back the result to player
				send(Connections[i], (char*)&p1Y, sizeof(p1Y), NULL);			//send back the result to player
				send(Connections[i], (char*)&p1Z, sizeof(p1Z), NULL);			//send back the result to player
			}
		}
		break;
	}
	default:
		std::cout << "Server// Recived unknown packet: " << packetid << " from client."<< std::endl;
		break;
	
	}
	return true;
}

void ClientHandlerThread(int index)										// the thread will handle the recv of packets and the disconnects if that occurs
{
	Packet packetID;													// instanciate packet
	while (true)
	{
		if (!GetPacketID(index, packetID))								// get packet id
			break;														// if error , exit this loop
		if (!ServerProcessIncomingPacket(index, packetID))				// if the packet was not precessed correctly
			break;														// break the client handler loop.
	}
	std::cout << "Lost connection to player " << index+1 << std::endl;
	if (index == 0)														// if we lost connection to player 1, set p1 bool to false so we can connect again as player 1
	{
		p1 = false;														// player 1 false
		std::cout << "p1:" << p1 << std::endl;
		awaitReconnect = true;
	}
	else if (index == 1)												// if we lost connection to player 2, set p1 bool to false so we can connect again as player 2
	{
		p2 = false;														// player 2 false
		std::cout << "p2:" << p2 << std::endl;
		awaitReconnect = true;
	}
	closesocket(Connections[index]);									// we should close connection when we are done with socket	
}

int main()
{
	serverRunning = true;

	//Winsock Startup
	WSADATA wsaData;																// create wsaData.
	WORD DllVersion = MAKEWORD(2, 1);												// Get winsock version, MAKEWORD(2,1) will get us winsock2.1.
	if (WSAStartup(DllVersion, &wsaData) != 0)										//if WSAStartup dont return 0, then an error has occured.
	{
		MessageBoxA(NULL, "Server// Winsock startup failed", "Error", MB_OK | MB_ICONERROR); // display a error box if WSASstartup dont run.
		exit(1);																	// exit application (if we return anything but 0 we will exit application).
	}
	else if(WSAStartup(DllVersion, &wsaData) == 0)									// if WSASstartup did run.
	{
		std::cout << "Server// Server online!" << std::endl;									// Tell the serveruser that server is online.
	}

	SOCKADDR_IN server_addr;														// Adress that we will bind our listening socket to.
	int addrlen = sizeof(server_addr);												// length of the adress.
	std::uint32_t ip_address;														// ??
	inet_pton(AF_INET, "192.168.1.101", &ip_address);									// The InetPton function converts an IPv4 or IPv6 Internet network address in its standard text presentation form into its numeric binary form.
	server_addr.sin_addr.s_addr = ip_address;										// ??
	server_addr.sin_port = htons(1111);												// port for the client to connect to.
	server_addr.sin_family = AF_INET;												// IPv4 Socket to change type type AF_INET6 = IPv6, alt (insert both).

	SOCKET sListen = socket(AF_INET, SOCK_STREAM, NULL);							// Create socket so server can listen to connection.
	bind(sListen, (SOCKADDR*)&server_addr, sizeof(server_addr));					// bind the adress to the socket.
	listen(sListen, SOMAXCONN);														// place sListen socket in a s´tate in whith it is listening to a incomin connection.

	// player connections
	SOCKET newConnection;															// socket to hold the new players connection to the server.
	for (int i = 0; i < 2; i++)
	{
		std::string s;																// the sring s is for ??
		newConnection = accept(sListen, (SOCKADDR*)&server_addr, &addrlen);			// accept a new connection.
		if (newConnection == 0)														// if accepting the client connection failed.
		{
			std::cout << "Server// failed to accept the client connection!" << std::endl;	// failed to accept a new connection to the server
		}
		else // if the client connects
		{
			std::cout << "Server// Client Connected!" << std::endl;						// Tell server that a user connected.
			Connections[i] = newConnection;												// Set current connection to the socket array
			if (i == 0)
			{
				int pID = 1;															// the players ID
				Packet sendNewPacketID = P_Connect;										// give packet id
				send(newConnection, (char*)&sendNewPacketID, sizeof(Packet), NULL);		// Send packet id
				send(newConnection, (char*)&pID, sizeof(int), NULL);					// send player ID to client
				send(Connections[i], (char*)&p1X, sizeof(p1X), NULL);					//send back the result to player
				send(Connections[i], (char*)&p1Y, sizeof(p1Y), NULL);					//send back the result to player
				send(Connections[i], (char*)&p1Z, sizeof(p1Z), NULL);					//send back the result to player
				send(Connections[i], (char*)&p2X, sizeof(p2X), NULL);					//send back the result to player
				send(Connections[i], (char*)&p2Y, sizeof(p2Y), NULL);					//send back the result to player
				send(Connections[i], (char*)&p2Z, sizeof(p2Z), NULL);					//send back the result to player
				p1 = true;																// tell server that player is active
				std::cout << "p1:" << p1 << std::endl;									// inform server of player active bool
			}

			if (i == 1)
			{
				int pID = 2;															// the players ID
				Packet sendNewPacketID = P_Connect;										// give packet id
				send(newConnection, (char*)&sendNewPacketID, sizeof(Packet), NULL);		// Send packet id
				send(newConnection, (char*)&pID, sizeof(int), NULL);					// send player ID to client
				send(Connections[i], (char*)&p1X, sizeof(p1X), NULL);					//send back the result to player
				send(Connections[i], (char*)&p1Y, sizeof(p1Y), NULL);					//send back the result to player
				send(Connections[i], (char*)&p1Z, sizeof(p1Z), NULL);					//send back the result to player
				send(Connections[i], (char*)&p2X, sizeof(p2X), NULL);					//send back the result to player
				send(Connections[i], (char*)&p2Y, sizeof(p2Y), NULL);					//send back the result to player
				send(Connections[i], (char*)&p2Z, sizeof(p2Z), NULL);					//send back the result to player
				p2 = true;																// tell server that player is active
				std::cout << "p2:" << p2 << std::endl;									// inform server of player active bool
			}

			ConnectionCounter += 1;														// +1 so next connection get another connection position in the array
			CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)ClientHandlerThread, (LPVOID)(i), NULL, NULL); // lisener for the new client connection	
		}
	}
	while (awaitReconnect || serverRunning)
	{
		//std::cout << "Server// !" << std::endl;
		if (p1 == false)
		{
			SOCKET newConnection;														// socket to hold the new players connection to the server.
			std::string s;																// the sring s is for ??
			newConnection = accept(sListen, (SOCKADDR*)&server_addr, &addrlen);			// accept a new connection.
			if (newConnection == 0)														// if accepting the client connection failed.
			{
				std::cout << "Server// failed to accept the client connection!" << std::endl;	// failed to accept a new connection to the server
			}
			else // if the client connects
			{
				std::cout << "Server// Client Connected!" << std::endl;					// Tell server that a user connected.
				Connections[0] = newConnection;											// Set current connection to the socket array
				int pID = 1;															// the players ID
				Packet sendNewPacketID = P_Connect;										// give packet id
				send(newConnection, (char*)&sendNewPacketID, sizeof(Packet), NULL);		// Send packet id
				send(newConnection, (char*)&pID, sizeof(int), NULL);					// send player ID to client
				send(newConnection, (char*)&p1X, sizeof(p1X), NULL);					//send back the result to player
				send(newConnection, (char*)&p1Y, sizeof(p1Y), NULL);					//send back the result to player
				send(newConnection, (char*)&p1Z, sizeof(p1Z), NULL);					//send back the result to player
				send(newConnection, (char*)&p2X, sizeof(p2X), NULL);					//send back the result to player
				send(newConnection, (char*)&p2Y, sizeof(p2Y), NULL);					//send back the result to player
				send(newConnection, (char*)&p2Z, sizeof(p2Z), NULL);					//send back the result to player
				p1 = true;																// tell server that player is active
				std::cout << "p1:" << p1 << std::endl;									// inform server of player active bool
				CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)ClientHandlerThread, (LPVOID)(0), NULL, NULL); // lisener for the new client connection
			}
		}
		else if (p2 == false)
		{
			SOCKET newConnection;														// socket to hold the new players connection to the server.
			std::string s;																// the sring s is for ??
			newConnection = accept(sListen, (SOCKADDR*)&server_addr, &addrlen);			// accept a new connection.
			if (newConnection == 0)														// if accepting the client connection failed.
			{
				std::cout << "Server// failed to accept the client connection!" << std::endl;	// failed to accept a new connection to the server
			}
			else // if the client connects
			{
				std::cout << "Server// Client Connected!" << std::endl;					// Tell server that a user connected.
				Connections[1] = newConnection;											// Set current connection to the socket array

				char WM[44] = ("Server// You connected to the game player 2");			// create a buffer with a message.
				int pID = 2;															// the players ID
				Packet sendNewPacketID = P_Connect;										//give packet id
				send(newConnection, (char*)&sendNewPacketID, sizeof(Packet), NULL);		// Send packet id
				send(newConnection, (char*)&pID, sizeof(int), NULL);					// send player ID to client
				send(newConnection, (char*)&p1X, sizeof(p1X), NULL);					//send back the result to player
				send(newConnection, (char*)&p1Y, sizeof(p1Y), NULL);					//send back the result to player
				send(newConnection, (char*)&p1Z, sizeof(p1Z), NULL);					//send back the result to player
				send(newConnection, (char*)&p2X, sizeof(p2X), NULL);					//send back the result to player
				send(newConnection, (char*)&p2Y, sizeof(p2Y), NULL);					//send back the result to player
				send(newConnection, (char*)&p2Z, sizeof(p2Z), NULL);					//send back the result to player
				p2 = true;																// tell server that player is active
				std::cout << "p2:" << p2 << std::endl;									// inform server of player active bool
				CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)ClientHandlerThread, (LPVOID)(1), NULL, NULL); // lisener for the new client connection
			}
		}
	}
	// end player connections
	if (serverRunning == true)
	{
		std::cout << "The loop has ended, close the server!" << std::endl;
		system("pause");																// pause the application so the window dont close and loose connection.
	}
	return 0;
}