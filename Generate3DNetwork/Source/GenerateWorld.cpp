
#include "../Include/GenerateWorld.h"

//David Test
#include <glm/gtc/quaternion.hpp> 
#include <glm/gtx/quaternion.hpp>
using namespace glm;
float move = 0.05f;
float spin = 0.05f;
Player m_player1;
Player m_player2;
Client client;

		// Start of Math and Camera variabels
mat4 myIdentityMatrix = mat4(1.0f);
	
		// Start of camera variables
// These variables determines the cameras placement inthe world as well its facing direction
vec3 cameraViewDirection(0, -0.5, -2);  // In what direction the camera is facing
vec3 cameraWorldPosition(14, 12, 9); // Where in the world the camera is located

// Function to set the cameras view
mat4 View = lookAt(
	cameraWorldPosition, 
	cameraWorldPosition + cameraViewDirection,
	vec3(0, 1, 0)  // Head is up (set to 0,-1,0 to look upside-down)
);

// Projection matrix : 45&deg; Field of View, 4:3 ratio, display range : 0.1 unit <-> 1000 units
// This function determiens how much field of view our camera has
mat4 Projection = perspective(
	radians(90.0f)	// The horizontal Field of View, in degrees : the amount of "zoom". Think "camera lens". Usually between 90� (extra wide) and 30� (quite zoomed in)
	, 4.0f / 3.0f,		// Aspect Ratio. Depends on the size of your window. Notice that 4/3 == 800/600 == 1280/960, sounds familiar ?
	0.1f,				// Near clipping plane. Keep as big as possible, or you'll get precision issues.
	100.0f);			// Far clipping plane. Keep as little as possible.

	// End of camera variables
					
		// Start of object math variables
// Model matrix : an identity matrix (model will be at the origin)
mat4 Model = mat4(1.0f);
// math variabels
mat4 myTranslationMatrix = translate(vec3(6.0f, 3.0f, -30.0f));	// Left Goal
mat4 myTranslationMatrix2 = translate(vec3(22.0f, 3.0f, -30.0f));		// Right Goal);
								// mat4 does =? , speed = 1.0f, which angle to rotate along Y axis
mat4 MyRotationMatrix = rotate(glm::mat4(1.0f), 1.0f, glm::vec3(0.0f, 1.0f, 0.0f));	// Left Goal
mat4 MyRotationMatrix2 = rotate(glm::mat4(1.0f), 0.0f, glm::vec3(0.0f, 0.0f, 0.0f));	// Right Goal
mat4 endGoalModel1 = myTranslationMatrix * MyRotationMatrix;
mat4 endGoalModel2 = myTranslationMatrix2; // * MyRotationMatrix2;
// Our ModelViewProjection : multiplication of our 3 matrices
// We need: A Projection, View, Model
mat4 endgoalMvp1 = Projection * View * endGoalModel1; // Remember, matrix multiplication is the other way around
mat4 endgoalMvp2 = Projection * View * endGoalModel2;

	// End of objects math variables
		// End of Math and Camera Variabels


		// Start of Content Objects variabels

// These are Cube verticies pos
static const GLfloat cubeOneVerts[] = {
	-1.0f,-1.0f,-1.0f, // triangle 1 : begin
	-1.0f,-1.0f, 1.0f,
	-1.0f, 1.0f, 1.0f, // triangle 1 : end
	1.0f, 1.0f,-1.0f, // triangle 2 : begin
	-1.0f,-1.0f,-1.0f,
	-1.0f, 1.0f,-1.0f, // triangle 2 : end
	1.0f,-1.0f, 1.0f,
	-1.0f,-1.0f,-1.0f,
	1.0f,-1.0f,-1.0f,
	1.0f, 1.0f,-1.0f,
	1.0f,-1.0f,-1.0f,
	-1.0f,-1.0f,-1.0f,
	-1.0f,-1.0f,-1.0f,
	-1.0f, 1.0f, 1.0f,
	-1.0f, 1.0f,-1.0f,
	1.0f,-1.0f, 1.0f,
	-1.0f,-1.0f, 1.0f,
	-1.0f,-1.0f,-1.0f,
	-1.0f, 1.0f, 1.0f,
	-1.0f,-1.0f, 1.0f,
	1.0f,-1.0f, 1.0f,
	1.0f, 1.0f, 1.0f,
	1.0f,-1.0f,-1.0f,
	1.0f, 1.0f,-1.0f,
	1.0f,-1.0f,-1.0f,
	1.0f, 1.0f, 1.0f,
	1.0f,-1.0f, 1.0f,
	1.0f, 1.0f, 1.0f,
	1.0f, 1.0f,-1.0f,
	-1.0f, 1.0f,-1.0f,
	1.0f, 1.0f, 1.0f,
	-1.0f, 1.0f,-1.0f,
	-1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f,
	-1.0f, 1.0f, 1.0f,
	1.0f,-1.0f, 1.0f
};

// These are Cube colors: One color for each vertex. They were generated randomly.
static const GLfloat g_color_buffer_data_random[] = {
	0.583f,  0.771f,  0.014f,
	0.609f,  0.115f,  0.436f,
	0.327f,  0.483f,  0.844f,
	0.822f,  0.569f,  0.201f,
	0.435f,  0.602f,  0.223f,
	0.310f,  0.747f,  0.185f,
	0.597f,  0.770f,  0.761f,
	0.559f,  0.436f,  0.730f,
	0.359f,  0.583f,  0.152f,
	0.483f,  0.596f,  0.789f,
	0.559f,  0.861f,  0.639f,
	0.195f,  0.548f,  0.859f,
	0.014f,  0.184f,  0.576f,
	0.771f,  0.328f,  0.970f,
	0.406f,  0.615f,  0.116f,
	0.676f,  0.977f,  0.133f,
	0.971f,  0.572f,  0.833f,
	0.140f,  0.616f,  0.489f,
	0.997f,  0.513f,  0.064f,
	0.945f,  0.719f,  0.592f,
	0.543f,  0.021f,  0.978f,
	0.279f,  0.317f,  0.505f,
	0.167f,  0.620f,  0.077f,
	0.347f,  0.857f,  0.137f,
	0.055f,  0.953f,  0.042f,
	0.714f,  0.505f,  0.345f,
	0.783f,  0.290f,  0.734f,
	0.722f,  0.645f,  0.174f,
	0.302f,  0.455f,  0.848f,
	0.225f,  0.587f,  0.040f,
	0.517f,  0.713f,  0.338f,
	0.053f,  0.959f,  0.120f,
	0.393f,  0.621f,  0.362f,
	0.673f,  0.211f,  0.457f,
	0.820f,  0.883f,  0.371f,
	0.982f,  0.099f,  0.879f
};

// These are Cube colors: Floor cubes
static const GLfloat g_color_buffer_data_floor[] = {
	0.6f,  0.6f,  0.6f,
	0.6f,  0.6f,  0.6f,
	0.6f,  0.6f,  0.6f,
	0.6f,  0.6f,  0.6f,
	0.6f,  0.6f,  0.6f,
	0.6f,  0.6f,  0.6f,
	0.6f,  0.6f,  0.6f,
	0.6f,  0.6f,  0.6f,
	0.6f,  0.6f,  0.6f,
	0.6f,  0.6f,  0.6f,
	0.6f,  0.6f,  0.6f,
	0.6f,  0.6f,  0.6f,
	0.6f,  0.6f,  0.6f,
	0.6f,  0.6f,  0.6f,
	0.6f,  0.6f,  0.6f,
	0.6f,  0.6f,  0.6f,
	0.6f,  0.6f,  0.6f,
	0.6f,  0.6f,  0.6f,
	0.6f,  0.6f,  0.6f,
	0.6f,  0.6f,  0.6f,
	0.6f,  0.6f,  0.6f,
	0.6f,  0.6f,  0.6f,
	0.6f,  0.6f,  0.6f,
	0.6f,  0.6f,  0.6f,
	0.6f,  0.6f,  0.6f,
	0.6f,  0.6f,  0.6f,
	0.6f,  0.6f,  0.6f,
	0.0f,  1.0f,  0.0f,
	0.0f,  1.0f,  0.0f,
	0.0f,  1.0f,  0.0f,
	0.0f,  1.0f,  0.0f,
	0.0f,  1.0f,  0.0f,
	0.0f,  1.0f,  0.0f,
	0.6f,  0.6f,  0.6f,
	0.6f,  0.6f,  0.6f,
	0.6f,  0.6f,  0.6f,
};

// These are Cube colors: End goal yellow cubes
static const GLfloat g_color_buffer_data_EndGoal[] = {
	1.0f,  1.0f,  0.0f,
	1.0f,  1.0f,  0.0f,
	1.0f,  1.0f,  0.0f,
	1.0f,  1.0f,  0.0f,
	1.0f,  1.0f,  0.0f,
	1.0f,  1.0f,  0.0f,
	1.0f,  1.0f,  0.0f,
	1.0f,  1.0f,  0.0f,
	1.0f,  1.0f,  0.0f,
	1.0f,  1.0f,  0.0f,
	1.0f,  1.0f,  0.0f,
	1.0f,  1.0f,  0.0f,
	1.0f,  1.0f,  0.0f,
	1.0f,  1.0f,  0.0f,
	1.0f,  1.0f,  0.0f,
	1.0f,  1.0f,  0.0f,
	1.0f,  1.0f,  0.0f,
	1.0f,  1.0f,  0.0f,
	1.0f,  1.0f,  0.0f,
	1.0f,  1.0f,  0.0f,
	1.0f,  1.0f,  0.0f,
	1.0f,  1.0f,  0.0f,
	1.0f,  1.0f,  0.0f,
	1.0f,  1.0f,  0.0f,
	1.0f,  1.0f,  0.0f,
	1.0f,  1.0f,  0.0f,
	1.0f,  1.0f,  0.0f,
	1.0f,  1.0f,  0.0f,
	1.0f,  1.0f,  0.0f,
	1.0f,  1.0f,  0.0f,
	1.0f,  1.0f,  0.0f,
	1.0f,  1.0f,  0.0f,
	1.0f,  1.0f,  0.0f,
	1.0f,  1.0f,  0.0f,
	1.0f,  1.0f,  0.0f,
	1.0f,  1.0f,  0.0f,
};

// These are lava colors
static const GLfloat g_color_buffer_data_red[] = {
	1.0f,  0.0f,  0.0f,
	1.0f,  0.0f,  0.0f,
	1.0f,  0.0f,  0.0f,
	1.0f,  0.0f,  0.0f,
	1.0f,  0.0f,  0.0f,
	1.0f,  0.0f,  0.0f,
	1.0f,  0.0f,  0.0f,
	1.0f,  0.0f,  0.0f,
	1.0f,  0.0f,  0.0f,
	1.0f,  0.0f,  0.0f,
	1.0f,  0.0f,  0.0f,
	1.0f,  0.0f,  0.0f,
	1.0f,  0.0f,  0.0f,
	1.0f,  0.0f,  0.0f,
	1.0f,  0.0f,  0.0f,
	1.0f,  0.0f,  0.0f,
	1.0f,  0.0f,  0.0f,
	1.0f,  0.0f,  0.0f,
	1.0f,  0.0f,  0.0f,
	1.0f,  0.0f,  0.0f,
	1.0f,  0.0f,  0.0f,
	1.0f,  0.0f,  0.0f,
	1.0f,  0.0f,  0.0f,
	1.0f,  0.0f,  0.0f,
	1.0f,  0.0f,  0.0f,
	1.0f,  0.0f,  0.0f,
	1.0f,  0.0f,  0.0f,
	1.0f,  0.0f,  0.0f,
	1.0f,  0.0f,  0.0f,
	1.0f,  0.0f,  0.0f,
	1.0f,  0.0f,  0.0f,
	1.0f,  0.0f,  0.0f,
	1.0f,  0.0f,  0.0f,
	1.0f,  0.0f,  0.0f,
	1.0f,  0.0f,  0.0f,
	1.0f,  0.0f,  0.0f,
};

		// End of Content Object Variabels

GenerateWorld::GenerateWorld()
{
}

GenerateWorld::~GenerateWorld()
{
}

void GenerateWorld::UpdateWorld()
{
	// Z-buffer
	glEnable(GL_DEPTH_TEST);		// Enable depth test
	glDepthFunc(GL_LESS);			// Accept fragment if it closer to the camera than the former one

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);		// clears screen before draw phase

	// use our shaders
	glUseProgram(programID);

	// Vertex Buffer config
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	// color buffer config
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	// Redraw and Update Yellow Cubes
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_color_buffer_data_EndGoal), g_color_buffer_data_EndGoal, GL_STATIC_DRAW);

	glUniformMatrix4fv(matrixID, 1, GL_FALSE, &endgoalMvp1[0][0]);
	glDrawArrays(GL_TRIANGLES, 0, 12 * 3);		// Starting from vertex 0; 12 vertices total -> 1 cube

	glUniformMatrix4fv(matrixID, 1, GL_FALSE, &endgoalMvp2[0][0]);
	glDrawArrays(GL_TRIANGLES, 0, 12 * 3);		// Starting from vertex 0; 12 vertices total -> 1 cube

	// Keep spinning?
	spin += 0.05f;
	MyRotationMatrix = rotate(spin, glm::vec3(1.0f, 1.0f, 1.0f));	// Left Goal
	endGoalModel1 = myTranslationMatrix * MyRotationMatrix;
	endGoalModel2 = myTranslationMatrix2 * MyRotationMatrix;
	endgoalMvp1 = Projection * View * endGoalModel1; // Remember, matrix multiplication is the other way around
	endgoalMvp2 = Projection * View * endGoalModel2; // Remember, matrix multiplication is the other way around

	// Redraw and UpdateGenerates floor
	GenerateXAmountOfCubes(16, vec3(0, 0, 0), 2);

	// Does player belong here?
	//Player m_player1;
	m_player1.UpdatePlayer();
	//Player m_player2;
	m_player2.UpdatePlayer();

	// Cleares and disabels
	glDisableVertexAttribArray(0);
	glfwSwapBuffers(window);
	glfwPollEvents();

	float moveSpeed = 0.05f;

	// Intention to move player cubes
	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
	{
		//std::cout << "client number: " << client.GetPlayerID() << std::endl;

		//std::cout << "Input// UP - Press" << std::endl;
		client.SendClientData(P_PlayerInput, 3);
		if (client.GetPlayerID() == 1)
		{
			m_player1.MovePlayer(vec3(0, 0, -moveSpeed));
			client.GivePos(m_player1.GetPlayerPos().x, m_player1.GetPlayerPos().y, m_player1.GetPlayerPos().z);
		}

		if (client.GetPlayerID() == 2)
		{
			m_player2.MovePlayer(vec3(0, 0, -moveSpeed));
			client.GivePos(m_player2.GetPlayerPos().x, m_player2.GetPlayerPos().y, m_player2.GetPlayerPos().z);
		}	
	}
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
	{
		//std::cout << "Input// Left - Press" << std::endl;
		client.SendClientData(P_PlayerInput, 1);
		if (client.GetPlayerID() == 1)
		{
			m_player1.MovePlayer(vec3(-moveSpeed, 0, 0));
			client.GivePos(m_player1.GetPlayerPos().x, m_player1.GetPlayerPos().y, m_player1.GetPlayerPos().z);
		}	

		if (client.GetPlayerID() == 2)
		{
			m_player2.MovePlayer(vec3(-moveSpeed, 0, 0));
			client.GivePos(m_player2.GetPlayerPos().x, m_player2.GetPlayerPos().y, m_player2.GetPlayerPos().z);
		}
			
	}
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
	{
		//std::cout << "Input// Down - Press" << std::endl;
		client.SendClientData(P_PlayerInput, 4);
		if (client.GetPlayerID() == 1)
		{
			m_player1.MovePlayer(vec3(0, 0, moveSpeed));
			client.GivePos(m_player1.GetPlayerPos().x, m_player1.GetPlayerPos().y, m_player1.GetPlayerPos().z);
		}
			
		if (client.GetPlayerID() == 2)
		{
			m_player2.MovePlayer(vec3(0, 0, moveSpeed));
			client.GivePos(m_player2.GetPlayerPos().x, m_player2.GetPlayerPos().y, m_player2.GetPlayerPos().z);
		}
			
	}
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
	{
		//std::cout << "Input// Right - Press" << std::endl;
		client.SendClientData(P_PlayerInput, 2);

		if (client.GetPlayerID() == 1)
		{
			m_player1.MovePlayer(vec3(moveSpeed, 0, 0));
			client.GivePos(m_player1.GetPlayerPos().x, m_player1.GetPlayerPos().y, m_player1.GetPlayerPos().z);
		}	

		if (client.GetPlayerID() == 2)
		{
			m_player2.MovePlayer(vec3(moveSpeed, 0, 0));
			client.GivePos(m_player2.GetPlayerPos().x, m_player2.GetPlayerPos().y, m_player2.GetPlayerPos().z);
		}
			
	}
}

int GenerateWorld::SetUpAndCreateWorld()
{

			// Start of Create Window
	// Check to see if glfw lib initilized correctly
	if (glfwInit() == false)
	{
		fprintf(stderr, "GLFW failed to initialize");
		return -1;
	}
	// Initiate windows settings
	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	
	// Create the window and sets its variabel
	window = glfwCreateWindow(m_width, m_height, "3D OPENGL", NULL, NULL);
	// Check to see if window was created correctly
	if (!window)
	{
		fprintf(stderr, "Window failed to create");
		glfwTerminate();
		return -1;
	}
	
	// Set current window
	glfwMakeContextCurrent(window);
	// Check to see if glew lib was initilized correctly
	glewExperimental = true;
	if (glewInit() != GLEW_OK)
	{
		fprintf(stderr, "failed to initialize GLEW");
		glfwTerminate();
		return -1;
	}

			// End of Create Window
	
			// Start of Create World
	// Vertex array
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);
	
	// Cube vertex buffer
	glGenBuffers(1, &vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeOneVerts), cubeOneVerts, GL_STATIC_DRAW);
	
	// Color attribute buffer
	glGenBuffers(1, &colorbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_color_buffer_data_random), g_color_buffer_data_random, GL_STATIC_DRAW);
	
	// Loads shaders unto our program to use them
	programID = m_shader.LoadShaderFromFile("Assets/vertexshader.txt", "Assets/fragmentshader.txt");
	
	// Get a handle for our "MVP" uniform but Only during the initialisation
	matrixID = glGetUniformLocation(programID, "MVP");

	// Spawn 3D objects: End Goal Cubes
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_color_buffer_data_EndGoal), g_color_buffer_data_EndGoal, GL_STATIC_DRAW);

	glUniformMatrix4fv(matrixID, 1, GL_FALSE, &endgoalMvp1[0][0]);
	glDrawArrays(GL_TRIANGLES, 0, 12 * 3);		// Starting from vertex 0; 12 vertices total -> 1 cube

	glUniformMatrix4fv(matrixID, 1, GL_FALSE, &endgoalMvp2[0][0]);
	glDrawArrays(GL_TRIANGLES, 0, 12 * 3);		// Starting from vertex 0; 12 vertices total -> 1 cube

	// Spawns a floor for our player cubes to move upon, 
	GenerateXAmountOfCubes(16, vec3(0, 0, 0), 2);

	// Does PLayer belong here?
	//Player m_player1;
	// Player 1			// Set start pos for player(s), hand them the camera projection and cameras view
	m_player1.SetUpPlayer(vec3(8, 2, -2), Projection, View);
	// Player 2
	m_player2.SetUpPlayer(vec3(24, 2, -2), Projection, View);

			// End of Create World

	return 0;
}

void GenerateWorld::GenerateXAmountOfCubes(int amount, vec3 startpos, float pacing)
{
	// Saves original startpos for later use
	orgStartPos = startpos;

	int map1[16][16] =					// the game floor
	{
		0,0,0,1,1,0,0,0 ,0,0,0,1,1,0,0,0,
		0,1,1,1,1,0,0,0 ,0,1,1,1,1,0,0,0,
		0,1,1,0,0,0,0,0 ,0,1,1,0,0,0,0,0,
		0,1,0,0,0,0,0,0 ,0,1,0,0,0,0,0,0,
		0,1,1,1,0,0,0,0 ,0,1,1,1,0,0,0,0,
		0,1,1,1,1,1,0,0 ,0,1,1,1,1,1,0,0,
		0,0,0,0,1,1,1,0 ,0,0,0,0,1,1,1,0,
		0,0,0,0,0,1,1,0 ,0,0,0,0,0,1,1,0,
		0,1,1,1,1,1,1,0 ,0,1,1,1,1,1,1,0,
		0,1,1,0,0,0,0,0 ,0,1,1,0,0,0,0,0,
		0,1,0,0,0,0,0,0 ,0,1,0,0,0,0,0,0,
		0,1,1,1,1,1,0,0 ,0,1,1,1,1,1,0,0,
		0,0,1,1,1,1,0,0 ,0,0,1,1,1,1,0,0,
		0,0,0,0,1,1,0,0 ,0,0,0,0,1,1,0,0,
		0,0,0,1,1,0,0,0 ,0,0,0,1,1,0,0,0,
		0,0,0,1,0,0,0,0 ,0,0,0,1,0,0,0,0,
	};
	// Loop draw
	for (int i = 0; i < amount; i++)
	{
		// increase the initial values at the end of each loop so next draw it moved
		startpos.z -= pacing;
		for (int j = 0; j < amount; j++)
		{
			/*if (j != 8)
			{*/
				if (map1[i][j] == 1)
				{
					glBufferData(GL_ARRAY_BUFFER, sizeof(g_color_buffer_data_floor), g_color_buffer_data_floor, GL_STATIC_DRAW);
					// Set initial loop function varbs
					myTranslationMatrix4 = translate(startpos);
					loopMvp = Projection * View * myTranslationMatrix4;

					// insert coloring / texuring here

					// send in initial values
					glUniformMatrix4fv(matrixID, 1, GL_FALSE, &loopMvp[0][0]);
					// draw call for objects, specify how many triangles to be drawn
					glDrawArrays(GL_TRIANGLES, 0, 12 * 3);		// Starting from vertex 0; 12 vertices total -> 1 cube

					
				}
				else if(map1[i][j] == 0)
				{
					glBufferData(GL_ARRAY_BUFFER, sizeof(g_color_buffer_data_red), g_color_buffer_data_red, GL_STATIC_DRAW);
					startpos.y = -1;
					// Set initial loop function varbs
					myTranslationMatrix4 = translate(startpos);
					loopMvp = Projection * View * myTranslationMatrix4;

					// insert coloring / texuring here

					// send in initial values
					glUniformMatrix4fv(matrixID, 1, GL_FALSE, &loopMvp[0][0]);
					// draw call for objects, specify how many triangles to be drawn
					glDrawArrays(GL_TRIANGLES, 0, 12 * 3);		// Starting from vertex 0; 12 vertices total -> 1 cube
					startpos.y = 0;
				}
			//}
				startpos.x += pacing; // increase the initial values at the end of each loop so next draw it moved

				/*std::cout << "x = " << startpos.x << "." << std::endl;
				std::cout << "z = " << startpos.z << "." << std::endl;*/
				//std::cout << "i = " << i << "." << std::endl;
			
			
			if (j == amount-1)
			{
				/*std::cout << "new i =  " << i<< "." << std::endl;*/
				// resets x to its original value for next line
				startpos.x = orgStartPos.x;
			}
		}
	}
}

glm::mat4 GenerateWorld::ReturnProjection()
{
	return Projection;
}

glm::mat4 GenerateWorld::ReturnView()
{
	return View;
}

void GenerateWorld::NewPlayer1pos(float x, float y, float z)
{
	m_player1.SetPlayerPos(x, y, z);
}
void GenerateWorld::NewPlayer2pos(float x, float y, float z)
{
	m_player2.SetPlayerPos(x, y, z);
}