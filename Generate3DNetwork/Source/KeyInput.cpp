// KeyInput.cpp
#pragma comment(lib, "winmm.lib")

#include "../Include/KeyInput.h"
#include "../Include/Client.h"
#include "../Include/Player.h"
#include <stdio.h>
#include <conio.h>
#include <iostream>


#define KB_UP 72
#define KB_DOWN 80
#define KB_LEFT 75
#define KB_RIGHT 77
#define KB_ESCAPE 27

KeyInput::KeyInput()
{

}
KeyInput::~KeyInput()
{

}

void KeyInput::GetArrowKeysInput()
{
	int KB_code = 0;
	Client m_client;
	Player m_player;


	/*while (KB_code != KB_ESCAPE)
	{*/
		if (_kbhit())
		{
			KB_code = _getch();
			//printf("KB_code = %i \n", KB_code);

			switch (KB_code)
			{
			case KB_LEFT: // left arrow
			{
				std::cout << "Input// Left - Press" << std::endl;
				m_client.SendClientData(P_PlayerInput,1);				// send packetID and arrow direction to server
				m_player.MovePlayer(glm::vec3(-0.01, 0, 0));
				break;
			}


			case KB_RIGHT: // right arrow
			{
				std::cout << "Input// Right - Press" << std::endl;
				m_client.SendClientData(P_PlayerInput,2);				// send packetID and arrow direction to server
				m_player.MovePlayer(glm::vec3(0.01, 0, 0));
				break;
			}


			case KB_UP: // ...
			{
				std::cout << "Input// Up - Press" << std::endl;
				m_client.SendClientData(P_PlayerInput,3);				// send packetID and arrow direction to server
				m_player.MovePlayer(glm::vec3(0, 0, -0.01));
				break;
			}


			case KB_DOWN: // ...
			{
				std::cout << "Input// Down - Press" << std::endl;
				m_client.SendClientData(P_PlayerInput,4);				// send packetID and arrow direction to server
				m_player.MovePlayer(glm::vec3(0, 0, 0.01));
				break;
			}


			}

		}
	/*}*/

}