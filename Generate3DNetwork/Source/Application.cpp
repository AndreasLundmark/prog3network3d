#include "Include\Client.h"
#include "Application.h"

GLuint LoadShaders(const char* vertex_file_path, const char* fragment_file_path)
{
	// Create the shaders
	GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

	// Read the Vertex Shader code from the file
	std::string VertexShaderCode;
	std::ifstream VertexShaderStream(vertex_file_path, std::ios::in);
	if (VertexShaderStream.is_open()) {
		std::string Line = "";
		while (getline(VertexShaderStream, Line))
			VertexShaderCode += "\n" + Line;
		VertexShaderStream.close();
	}
	else {
		printf("Impossible to open %s. Are you in the right directory ? Don't forget to read the FAQ !\n", vertex_file_path);
		getchar();
		return 0;
	}

	// Read the Fragment Shader code from the file
	std::string FragmentShaderCode;
	std::ifstream FragmentShaderStream(fragment_file_path, std::ios::in);
	if (FragmentShaderStream.is_open()) {
		std::string Line = "";
		while (getline(FragmentShaderStream, Line))
			FragmentShaderCode += "\n" + Line;
		FragmentShaderStream.close();
	}

	GLint Result = GL_FALSE;
	int InfoLogLength;

	// Compile Vertex Shader
	printf("Compiling shader : %s\n", vertex_file_path);
	char const * VertexSourcePointer = VertexShaderCode.c_str();
	glShaderSource(VertexShaderID, 1, &VertexSourcePointer, NULL);
	glCompileShader(VertexShaderID);

	// Check Vertex Shader
	glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if (InfoLogLength > 0) {
		std::vector<char> VertexShaderErrorMessage(InfoLogLength + 1);
		glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
		printf("%s\n", &VertexShaderErrorMessage[0]);
	}

	// Compile Fragment Shader
	printf("Compiling shader : %s\n", fragment_file_path);
	char const * FragmentSourcePointer = FragmentShaderCode.c_str();
	glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer, NULL);
	glCompileShader(FragmentShaderID);

	// Check Fragment Shader
	glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if (InfoLogLength > 0) {
		std::vector<char> FragmentShaderErrorMessage(InfoLogLength + 1);
		glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
		printf("%s\n", &FragmentShaderErrorMessage[0]);
	}

	// Link the program
	printf("Linking program\n");
	GLuint shaderProgramID = glCreateProgram();
	glAttachShader(shaderProgramID, VertexShaderID);
	glAttachShader(shaderProgramID, FragmentShaderID);
	glLinkProgram(shaderProgramID);

	// Check the program
	glGetProgramiv(shaderProgramID, GL_LINK_STATUS, &Result);
	glGetProgramiv(shaderProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if (InfoLogLength > 0) {
		std::vector<char> ProgramErrorMessage(InfoLogLength + 1);
		glGetProgramInfoLog(shaderProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
		printf("%s\n", &ProgramErrorMessage[0]);
	}


	glDetachShader(shaderProgramID, VertexShaderID);
	glDetachShader(shaderProgramID, FragmentShaderID);

	glDeleteShader(VertexShaderID);
	glDeleteShader(FragmentShaderID);

	return shaderProgramID;
}

GLuint LoadBmpFile(const char* filename)
{
	// Data read from the header of the BMP file
	unsigned char header[54]; // Each BMP file begins by a 54-bytes header
	unsigned int dataPos;     // Position in the file where the actual data begins
	unsigned int width, height;
	unsigned int imageSize;   // = width*height*3
	unsigned char * data;	  // Actual RGB data
	
	FILE * file = fopen(filename, "rb");
	if (!file)		// can we open the texture file
	{ 
		printf("Image could not be opened\n"); return 0; 
	}

	if (fread(header, 1, 54, file) != 54) // is this really a bmp file. If not 54 bytes read : problem
	{ 
		printf("Not a correct BMP file\n");
		return false;
	}

	if (header[0] != 'B' || header[1] != 'M') // checks if file is corrupt
	{
		printf("Not a correct BMP file\n");
		return 0;
	}

	// Read ints from the byte array
	dataPos = *(int*)&(header[0x0A]);
	imageSize = *(int*)&(header[0x22]);
	width = *(int*)&(header[0x12]);
	height = *(int*)&(header[0x16]);

	// Some BMP files are misformatted, guess missing information
	if (imageSize == 0)    imageSize = width*height * 3; // 3 : one byte for each Red, Green and Blue component
	if (dataPos == 0)      dataPos = 54; // The BMP header is done that way

	// Create a buffer
	data = new unsigned char[imageSize];

	// Read the actual data from the file into the buffer
	fread(data, 1, imageSize, file);

	//Everything is in memory now, the file can be closed
	fclose(file);

	// Create one OpenGL texture
	GLuint textureID;
	glGenTextures(1, &textureID);

	// "Bind" the newly created texture : all future texture functions will modify this texture
	glBindTexture(GL_TEXTURE_2D, textureID);

	// Give the image to OpenGL
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, data);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	return textureID;
}

int Generate3DWorld()
{
	//client
	Client m_client;
	//m_client.ConnectToGame();
	// end client


	// check to see if glfw lib initilized correctly
	if (glfwInit() == false)
	{
		fprintf(stderr, "GLFW failed to initialize");
		return -1;
	}
	// initiate windows settings
	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// create the window and sets its variabel
	GLFWwindow* window;
	window = glfwCreateWindow(m_width, m_height, "3D OPENGL", NULL, NULL);
	// check to see if window was created correctly
	if (!window)
	{
		fprintf(stderr, "Window failed to create");
		glfwTerminate();
		return -1;
	}

	// set current window
	glfwMakeContextCurrent(window);
	// check to see if glew lib was initilized correctly
	glewExperimental = true;
	if (glewInit() != GLEW_OK)
	{
		fprintf(stderr, "failed to initialize GLEW");
		glfwTerminate();
		return -1;
	}

	// vertex array
	GLuint VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);

	// cube vertex buffer
	GLuint vertexBuffer;
	glGenBuffers(1, &vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeOneVerts), cubeOneVerts, GL_STATIC_DRAW);

	// triangle vertex buffer
	GLuint planeVertexBuffer;
	glGenBuffers(1, &planeVertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, planeVertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(planeVerts), planeVerts, GL_STATIC_DRAW);

	// color attribute buffer
	GLuint colorbuffer;
	glGenBuffers(1, &colorbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_color_buffer_data), g_color_buffer_data, GL_STATIC_DRAW);

	// loads shaders unto our program to use them
	GLuint programID = LoadShaders("Assets/vertexshader.txt", "Assets/fragmentshader.txt");

	// Get a handle for our "MVP" uniform but Only during the initialisation
	GLuint matrixID = glGetUniformLocation(programID, "MVP");

	//Window stays open until player hit ESC-key or presses X
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
	do //main loop (its a while loop)
	{
		// Z-buffer
		glEnable(GL_DEPTH_TEST);		// Enable depth test
		glDepthFunc(GL_LESS);			// Accept fragment if it closer to the camera than the former one

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);		// clears screen before draw phase

				// 1:st draw 
		// 1:st content objects matrix
		glUniformMatrix4fv(matrixID, 1, GL_FALSE, &mvp1[0][0]);
		// use our shaders
		glUseProgram(programID);

		// Vertex Buffer config
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

		// color buffer config
		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	
		// 1:st draw call for objects, specify how many triangles to be drawn
		glDrawArrays(GL_TRIANGLES, 0, 12*3);		// Starting from vertex 0; 12 vertices total -> 1 cube

			// 2:nd draw - new drawcall and new MVP (to set transtlate etc)
		glUniformMatrix4fv(matrixID, 1, GL_FALSE, &mvp2[0][0]);
		glDrawArrays(GL_TRIANGLES, 0, 12 * 3);		// Starting from vertex 0; 12 vertices total -> 1 cube

			// 3:e draw
		glBindBuffer(GL_ARRAY_BUFFER, planeVertexBuffer);
		glUniformMatrix4fv(matrixID, 1, GL_FALSE, &mvp3[0][0]);
		glDrawArrays(GL_TRIANGLES, 0, 3);		// Starting from vertex 0; 3 vertices total -> 1 triangle

			// cleares and disabels
		glDisableVertexAttribArray(0);
		glfwSwapBuffers(window);
		glfwPollEvents();

		if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
		{
			mvp2 += glm::translate(glm::vec3(0.0f, 0.0f, -0.2f));
		}		
		if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
		{
			mvp2 -= glm::translate(glm::vec3(0.0f, 0.0f, 0.2f));
		}
	}

	while (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS && glfwWindowShouldClose(window) == 0);
}

int main(int argc, char** argv)
{
	Generate3DWorld();

	return 0;
}