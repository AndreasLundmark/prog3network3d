﻿//client.cpp

#pragma comment(lib,"ws2_32.lib")
#include <ws2tcpip.h>
#include <WinSock2.h>
#include <iostream>
#include "../Include/Client.h"
#include "../Include/GenerateWorld.h"

SOCKET Connection; // create connection socket.

bool debugAll = false; // just a bool to turn on and of extra couts with information
int packetId = 0; // old packet id

float pPosX;
float pPosY;
float pPosZ;

bool winBool = false;

Client m_client; // cast to itself 
GenerateWorld gworld;

Client::Client()
{
	playerID = 0; // this clients id
}

Client::~Client()
{

}

void Client::WinsockStartup()
{
	//Winsock Startup.
	WSADATA wsaData;																			// create wsaData.
	WORD DllVersion = MAKEWORD(2, 1);															// Get winsock version, MAKEWORD(2,1) will get us winsock2.1.
	if (WSAStartup(DllVersion, &wsaData) != 0)													//if WSAStartup dont return 0, then an error has occured.
	{
		MessageBoxA(NULL, "Client// Winsock startup failed", "Error", MB_OK | MB_ICONERROR);				// display a error box if WSASstartup dont run.
		exit(1);																				// exit application (if we return anything but 0 we will exit application).
	}
	// end if problem, and dont run anything below.

	if (debugAll == true)
		std::cout << "Client// Done - winsockStartup" << std::endl;
}

void Client::ConnectClientToServer(std::string IP)
{
	SOCKADDR_IN addr;																			// Adress to be binded to the connection socket.
	int sizeofaddr = sizeof(addr);																// length of the adress for the connect function.
	std::uint32_t ip_address;																	// create ip adress value
	inet_pton(AF_INET, IP.c_str(), &ip_address);												// The InetPton function converts an IPv4 or IPv6 Internet network address in its standard text presentation form into its numeric binary form.
	addr.sin_addr.s_addr = ip_address;															// ??
	addr.sin_port = htons(1111);																// port for the client to connect to.
addr.sin_family = AF_INET;																	// IPv4 Socket to change type type AF_INET6 = IPv6, alt (insert both).

Connection = socket(AF_INET, SOCK_STREAM, NULL);											// Set connection socket.
if (connect(Connection, (SOCKADDR*)&addr, sizeofaddr) != 0)									// if we are unable to connect.
{
	MessageBoxA(NULL, "Client// Failed to connect to server!", "Error", MB_OK | MB_ICONERROR);		// display a error box if client dont find or cant connect to server.
	exit(1);
}

if (debugAll == true)
std::cout << "Client// Done - connect to server" << std::endl;

}

//--! CennectToGame function !--//
int Client::ConnectToGame(std::string gameIP)
{
	WinsockStartup();
	ConnectClientToServer(gameIP);																// Connect client to the server and tell us that we have connected.

	CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)ReceiveClientThread, NULL, NULL, NULL);	// Create a thread that listen to the server.																		// Send message/data to server.
	if (debugAll == true)
		std::cout << "Client// Done - Setting up listeners" << std::endl;																// communication between server and client function
	if (debugAll == true)
		std::cout << "Client// Done with setup - connect to game" << std::endl;
	return 0;
}
//--! End CennectToGame function !--//

bool Client::ClientProcessIncomingPacket(Packet packetid) // precess incomig packet, if it was sucessful then return true
{
	switch (packetid)
	{
	case P_Connect:
	{
		int pID = 0;
		float newpPosX;
		float newpPosY;
		float newpPosZ;
		float newpPosX2;
		float newpPosY2;
		float newpPosZ2;
		if (playerID == 0) // only accept new id if we have none
		{
			recv(Connection, (char*)&pID, sizeof(pID), NULL);											// recive message from the sever to this buffer
			std::cout << std::endl << "Server// your player ID = " << pID << std::endl;					// display the message recived from the server to the player

			m_client.SetPlayerID(pID);
			std::cout << std::endl << "Client// Confirm player ID = " << m_client.GetPlayerID() << std::endl;

			recv(Connection, (char*)&newpPosX, sizeof(newpPosX), NULL);
			recv(Connection, (char*)&newpPosY, sizeof(newpPosY), NULL);
			recv(Connection, (char*)&newpPosZ, sizeof(newpPosZ), NULL);
			recv(Connection, (char*)&newpPosX2, sizeof(newpPosX), NULL);
			recv(Connection, (char*)&newpPosY2, sizeof(newpPosY), NULL);
			recv(Connection, (char*)&newpPosZ2, sizeof(newpPosZ), NULL);

			if (playerID != 2)													// if this client is not player 2
			{		
					gworld.NewPlayer1pos(newpPosX, newpPosY, newpPosZ);			// change player 1 cube position after server value
					gworld.NewPlayer2pos(newpPosX2, newpPosY2, newpPosZ2);		// change player 2 cube position after server value
			}
			if (playerID != 1)													// if this client is not player 1
			{
					gworld.NewPlayer2pos(newpPosX2, newpPosY2, newpPosZ2);			// change player 2 cube position after server value
					gworld.NewPlayer1pos(newpPosX, newpPosY, newpPosZ);		// change player 1 cube position after server value
			}
		}
			//// prepare to send ip and id to sever
			//packetId = P_Connect;
	}
	case P_PlayerInput:
	{
		int respondPlayerID;
		int respondArrowDirection;
		bool sendReady = false;
		packetId = P_PlayerInput;
		recv(Connection, (char*)&sendReady, sizeof(sendReady), NULL);
		if (sendReady == true)
		{
			if (debugAll == true)
				std::cout << "Server// confirm packet: " << packetId << ". from player";
			recv(Connection, (char*)&respondPlayerID, sizeof(respondPlayerID), NULL);
			if (debugAll == true)
				std::cout << respondPlayerID << ". with arrow direction ";
			recv(Connection, (char*)&respondArrowDirection, sizeof(respondArrowDirection), NULL);
			if (debugAll == true)
				std::cout << respondArrowDirection << "." << std::endl << std::endl;
			sendReady = false;
		}
		break;
	}
	case P_GameState:
	{
		int respondPlayerID;
		float newpPosX;
		float newpPosY;
		float newpPosZ;
		// the other pos values are for this clients player cube
		float newOtherpPosX;
		float newOtherpPosY;
		float newOtherpPosZ;
		packetId = P_GameState;
		if (debugAll == true)
			std::cout << "Server// confirm packet: " << packetId << ". from player";
		recv(Connection, (char*)&respondPlayerID, sizeof(respondPlayerID), NULL);
		if (debugAll == true)
			std::cout << respondPlayerID << ". with new position ";
		recv(Connection, (char*)&newpPosX, sizeof(newpPosX), NULL);
		recv(Connection, (char*)&newpPosY, sizeof(newpPosY), NULL);
		recv(Connection, (char*)&newpPosZ, sizeof(newpPosZ), NULL);
		//this clients position from server
		recv(Connection, (char*)&newOtherpPosX, sizeof(newOtherpPosX), NULL);
		recv(Connection, (char*)&newOtherpPosY, sizeof(newOtherpPosY), NULL);
		recv(Connection, (char*)&newOtherpPosZ, sizeof(newOtherpPosZ), NULL);

		if (playerID != 1)													// if this client is not player 1
		{
			if (respondPlayerID != playerID)								// and if the packet was not from this client originally
			{
				gworld.NewPlayer1pos(newpPosX, newpPosY, newpPosZ);			// change player 1 cube position
			}
			//// input prediction for player 2
			if (respondPlayerID == playerID)									// if the client is player 2
			{
				gworld.NewPlayer2pos(newOtherpPosX, newOtherpPosY, newOtherpPosZ);
				if (debugAll == true)
				std::cout << "!difference! " << pPosX << " and " << newOtherpPosX << std::endl;
			}
		}
		if (playerID != 2)													// if this client is not player 2
		{
			if (respondPlayerID != playerID)								// and if the packet was not from this client originally
			{
				gworld.NewPlayer2pos(newpPosX, newpPosY, newpPosZ);			// change player 2 cube position
			}
			//// input prediction for player 1
			if (respondPlayerID == playerID)									// if the client is player 1
			{
				gworld.NewPlayer1pos(newOtherpPosX, newOtherpPosY, newOtherpPosZ);
				if (debugAll == true)
				std::cout << "!difference! " << pPosX << " and " << newOtherpPosX << std::endl;
			}
		}
		if (debugAll == true)
		std::cout << std::endl << "player" << respondPlayerID << " new pos: " << newpPosX << " " << newpPosY << " " << newpPosX << ".!" << std::endl;
		break;
	}
	default:
	{
		if (debugAll == true)
			std::cout << "Client// Recived unknown packet: " << packetid << " from server." << std::endl;
		break;
	}
	}
	return true;
}

//--! ClientThread function !--//
void Client::ReceiveClientThread()
{
	Packet packetid;
	while (true)																				// if the client is running 
	{
		if (!GetPacketID(packetid))																// get packet id
			break;																				// if there is an issue getting packet, exit this loop
		if (!m_client.ClientProcessIncomingPacket(packetid))									// if the packet was not precessed correctly
			break;																				// break the client handler loop.
	}
	std::cout << "Lost connection to Host server." << std::endl;								// tell client that connection was lost
	closesocket(Connection);																	// we should close connection when we are done with socket
} 
//--! ClientThread function !--//

bool Client::SendClientData(Packet packId,int direction) //!!!!!!!!!!! this is the reason to the game not starting !!!!!!//////
{
		Packet packetID = packId;																		// Packet id determins what the reciver should collect (1 = serverMessage, 2 = arrowKeysInput, 3 = gameBools)
		
		if (packetID == P_PlayerInput)
		{
			int arrowDirection = direction;																// give the packet arrow input
			int playerId = m_client.GetPlayerID();														// give this clients id so the server know what player sent the packet
			send(Connection, (char*)&packetID, sizeof(Packet), NULL);									// send player id so the reciver know how to collect the information	
			send(Connection, (char*)&playerId, sizeof(int), NULL);										// send packet so the reciver know how to collect the information	
			int ReturnCheck = send(Connection, (char*)&arrowDirection, sizeof(int), NULL);				// send packet so the reciver know how to collect the information	
			if (ReturnCheck == SOCKET_ERROR)															// send an error if a socket dont recive the right information
				return false;
			if(debugAll == true)
			std::cout << "Client// p" << m_client.GetPlayerID()<< " sent packet " << packetID << " with the ArrowKey: " << arrowDirection << "." << std::endl;
		}	
		return true;
}

bool Client::SendPacketID(Packet _packetid)
{
	int ReturnCheck = send(Connection, (char*)&_packetid, sizeof(Packet), NULL);						// send packet so the reciver know how to collect the information	
	if (ReturnCheck == SOCKET_ERROR)																	// send an error if a socket dont recive the right information
		return false;
	return true;
}

bool Client::GetPacketID(Packet &_packetid)
{
	int ReturnCheck = recv(Connection, (char*)&_packetid, sizeof(Packet), NULL);						// send packet so the reciver know how to collect the information	
	if (ReturnCheck == SOCKET_ERROR)																	// send an error if a socket dont recive the right information
		return false;
	return true;
}

void Client::SetPlayerID(int id)
{
	m_client.playerID = id;
}

int Client::GetPlayerID()
{
	return m_client.playerID;
}

void Client::GivePos(float x, float y, float z)
{
	pPosX = x;
	pPosY = y;
	pPosZ = z;

	if (debugAll == true)
	std::cout << std::endl << "!!!confirm pos: " << pPosX << " " << pPosY << " " << pPosZ << " "  << std::endl;

	checkPlayerRacePosition(x, y, z);

	Packet packetID;																		// Packet id determins what the reciver should collect (1 = serverMessage, 2 = arrowKeysInput, 3 = gameBools)

	packetID = P_GameState;
	int playerId = m_client.GetPlayerID();														// give this clients id so the server know what player sent the packet
	send(Connection, (char*)&packetID, sizeof(Packet), NULL);									// send player id so the reciver know how to collect the information	
	send(Connection, (char*)&playerId, sizeof(int), NULL);										// send packet so the reciver know how to collect the information	
	send(Connection, (char*)&pPosX, sizeof(pPosX), NULL);
	send(Connection, (char*)&pPosY, sizeof(pPosY), NULL);
	send(Connection, (char*)&pPosZ, sizeof(pPosZ), NULL);

	
}

void Client::checkPlayerRacePosition(float x, float y, float z)
{
	// check if the player is in the goal
	if ((x > 5 && x < 7) && m_client.GetPlayerID() == 1)										// player have lined up with the goal
	{
		if (z < (-29.5))																			// the player hit goal
		{
			if (winBool == false)
			{
				winBool = true;
				std::cout << "You are in the goal!" << std::endl;
			}
		}
	}
	else if ((x > 21 && x < 23) && m_client.GetPlayerID() == 2)									// player have lined up with the goal
	{
		if (z<(-29.5))																			// the player hit goal
		{
			if (winBool == false)
			{
				winBool = true;
				std::cout << "You are in the goal!" << std::endl;
			}
		}
	}
	// check if the player is out of bounds
	//if (m_client.GetPlayerID() == 1)
	//{
	//	if (z > -3 && z < -1)							//row 1 in map +- half a player
	//	{
	//		if (x < 5 || x > 9)
	//		{
	//			std::cout << "outside row 1" << z << std::endl;
	//			pPosY = -0.8f;
	//		}
	//	}
	//	if (z > -5 && z < -3)							//row 1 in map +- half a player
	//	{
	//		if (x < 1 || x > 5)
	//		{
	//			std::cout << "outside row 1" << z << std::endl;
	//			pPosY = -0.8f;
	//		}
	//	}
	//}
}

