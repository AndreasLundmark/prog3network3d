
#include "../Include/Player.h"
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

bool loacalPlayer = false;

glm::vec3 currentPos;
GenerateWorld m_world;

glm::mat4 newProjection;
glm::mat4 newView;
glm::mat4 playerTranslationMatrix;
glm::mat4 playerMvp;

// Creates an identity quaternion (no rotation)
glm::quat MyQuaternion;

using namespace glm;

// PLayer Cube Verts
static const GLfloat playerCube_Vertrices[] = {
	-1.0f,-1.0f,-1.0f, // triangle 1 : begin
	-1.0f,-1.0f, 1.0f,
	-1.0f, 1.0f, 1.0f, // triangle 1 : end
	1.0f, 1.0f,-1.0f, // triangle 2 : begin
	-1.0f,-1.0f,-1.0f,
	-1.0f, 1.0f,-1.0f, // triangle 2 : end
	1.0f,-1.0f, 1.0f,
	-1.0f,-1.0f,-1.0f,
	1.0f,-1.0f,-1.0f,
	1.0f, 1.0f,-1.0f,
	1.0f,-1.0f,-1.0f,
	-1.0f,-1.0f,-1.0f,
	-1.0f,-1.0f,-1.0f,
	-1.0f, 1.0f, 1.0f,
	-1.0f, 1.0f,-1.0f,
	1.0f,-1.0f, 1.0f,
	-1.0f,-1.0f, 1.0f,
	-1.0f,-1.0f,-1.0f,
	-1.0f, 1.0f, 1.0f,
	-1.0f,-1.0f, 1.0f,
	1.0f,-1.0f, 1.0f,
	1.0f, 1.0f, 1.0f,
	1.0f,-1.0f,-1.0f,
	1.0f, 1.0f,-1.0f,
	1.0f,-1.0f,-1.0f,
	1.0f, 1.0f, 1.0f,
	1.0f,-1.0f, 1.0f,
	1.0f, 1.0f, 1.0f,
	1.0f, 1.0f,-1.0f,
	-1.0f, 1.0f,-1.0f,
	1.0f, 1.0f, 1.0f,
	-1.0f, 1.0f,-1.0f,
	-1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f,
	-1.0f, 1.0f, 1.0f,
	1.0f,-1.0f, 1.0f
};

// PLayer Cube Color
static const GLfloat blue_color_buffer_data[] = {
	0.0f,  0.0f,  1.771f,
	0.0f,  0.0f,  1.115f,
	0.0f,  0.0f,  1.483f,
	0.0f,  0.0f,  1.569f,
	0.0f,  0.0f,  1.602f,
	0.0f,  0.0f,  1.747f,
	0.0f,  0.0f,  1.770f,
	0.0f,  0.0f,  1.436f,
	0.0f,  0.0f,  1.583f,
	0.0f,  0.0f,  1.596f,
	0.0f,  0.0f,  1.861f,
	0.0f,  0.0f,  1.548f,
	0.0f,  0.0f,  1.184f,
	0.0f,  0.0f,  1.328f,
	0.0f,  0.0f,  1.615f,
	0.0f,  0.0f,  1.977f,
	0.0f,  0.0f,  1.572f,
	0.0f,  0.0f,  1.616f,
	0.0f,  0.0f,  1.513f,
	0.0f,  0.0f,  1.719f,
	0.0f,  0.0f,  1.021f,
	0.0f,  0.0f,  1.317f,
	0.0f,  0.0f,  1.620f,
	0.0f,  0.0f,  1.857f,
	0.0f,  0.0f,  1.953f,
	0.0f,  0.0f,  1.505f,
	0.0f,  0.0f,  1.290f,
	0.0f,  0.0f,  1.645f,
	0.0f,  0.0f,  1.455f,
	0.0f,  0.0f,  1.587f,
	0.0f,  0.0f,  1.713f,
	0.0f,  0.0f,  1.959f,
	0.0f,  0.0f,  1.621f,
	0.0f,  0.0f,  1.211f,
	0.0f,  0.0f,  1.883f,
	0.0f,  0.0f,  1.099f
};

Player::Player()
{
	originPos = vec3(0, 0, 0);
	m_speed = 1.0f;

	//std::cout << "initial org pos =  " <<  originPos.x << "." << originPos.y << "." << originPos.z << "." << std::endl;
	// Loads shaders unto our program to use them
	//player_programID = m_shader.LoadShaderFromFile("Assets/vertexshader.txt", "Assets/fragmentshader.txt");

	// Get a handle for our "MVP" uniform but Only during the initialisation
	//player_matrixID = glGetUniformLocation(player_programID, "MVP");

	
	// Don't forget to #include <glm/gtc/quaternion.hpp> and <glm/gtx/quaternion.hpp>

	//// Direct specification of the 4 components
	//// You almost never use this directly
	//MyQuaternion = quat(0, 1, 0, 0); //(w,x,y,z)

	//// Conversion from Euler angles (in radians) to Quaternion
	//vec3 EulerAngles(90, 45, 0);
	//MyQuaternion = quat(EulerAngles);

	//// Conversion from axis-angle
	//// In GLM the angle must be in degrees here, so convert it.
	//float RotationAngle = 10.0f;
	//float RotationAxis = 10.0f;
	//MyQuaternion = gtx::quaternion::angleAxis(degrees(RotationAngle), RotationAxis);
}

Player::~Player()
{

}

void Player::UpdatePlayer()
{
	player_projection = player_projection;
	player_view = player_view;
	//std::cout << "new org pos =  " << originPos.x << "." << originPos.y << "." << originPos.z << "." << std::endl;
	
	playerTranslationMatrix = translate(originPos);
	playerMvp = player_projection * player_view * playerTranslationMatrix;
	
	// color attribute buffer
	glGenBuffers(1, &player_colorbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, player_colorbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(blue_color_buffer_data), blue_color_buffer_data, GL_STATIC_DRAW);
	
	// color buffer config
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, player_colorbuffer);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	
	// send in initial values
	glUniformMatrix4fv(player_matrixID, 1, GL_FALSE, &playerMvp[0][0]);
	// draw call for objects, specify how many triangles to be drawn
	glDrawArrays(GL_TRIANGLES, 0, 12 * 3);		// Starting from vertex 0; 12 vertices total -> 1 cube

				// 2:nd Try bro

	//// Z-buffer
	//glEnable(GL_DEPTH_TEST);		// Enable depth test
	//glDepthFunc(GL_LESS);			// Accept fragment if it closer to the camera than the former one
	//
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);		// clears screen before draw phase
	//
	//// Use our shaders
	//glUseProgram(player_programID);
	//
	//// Vertex Buffer config
	//glEnableVertexAttribArray(0);
	//glBindBuffer(GL_ARRAY_BUFFER, player_vertexBuffer);
	//glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	//
	//// color buffer config
	//glEnableVertexAttribArray(1);
	//glBindBuffer(GL_ARRAY_BUFFER, player_colorbuffer);
	//glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	//
	//// Redraw player
	//glBufferData(GL_ARRAY_BUFFER, sizeof(blue_color_buffer_data), blue_color_buffer_data, GL_STATIC_DRAW);
	//glUniformMatrix4fv(player_matrixID, 1, GL_FALSE, &playerMvp[0][0]);
	//glDrawArrays(GL_TRIANGLES, 0, 12 * 3);		// Starting from vertex 0; 12 vertices total -> 1 cube
	//
	//// Cleares and disabels
	//glDisableVertexAttribArray(0);
	//glfwSwapBuffers(window);
	//glfwPollEvents();
}

int Player::SetUpPlayer(vec3 spawnPos, mat4 projection, mat4 view)
{
	// set origin to spawnpos
	originPos = spawnPos;
	player_projection = projection;
	player_view = view;
	//std::cout << "new org pos =  " << originPos.x << "." << originPos.y << "." << originPos.z << "." << std::endl;
	
	playerTranslationMatrix = translate(spawnPos);
	playerMvp = projection * view * playerTranslationMatrix;
	
	newProjection = projection;
	newView = view;
	
	// color attribute buffer
	glGenBuffers(1, &player_colorbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, player_colorbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(blue_color_buffer_data), blue_color_buffer_data, GL_STATIC_DRAW);
	
	// color buffer config
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, player_colorbuffer);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	
	// send in initial values
	glUniformMatrix4fv(player_matrixID, 1, GL_FALSE, &playerMvp[0][0]);
	// draw call for objects, specify how many triangles to be drawn
	glDrawArrays(GL_TRIANGLES, 0, 12 * 3);		// Starting from vertex 0; 12 vertices total -> 1 cube
	
	currentPos = spawnPos;
	
	//std::cout << " drawn player " << std::endl;

			// 2:nd Try bro

	//// cube maths
	//player_projection = projection;
	//player_view = view;
	//playerTranslationMatrix = translate(spawnPos);
	//playerMvp = projection * view * playerTranslationMatrix;
	//
	//// Vertex array
	//glGenVertexArrays(1, &player_VertexArrayID);
	//glBindVertexArray(player_VertexArrayID);
	//
	//// Cube vertex buffer
	//glGenBuffers(1, &player_vertexBuffer);
	//glBindBuffer(GL_ARRAY_BUFFER, player_vertexBuffer);
	//glBufferData(GL_ARRAY_BUFFER, sizeof(playerCube_Vertrices), playerCube_Vertrices, GL_STATIC_DRAW);
	//
	//// Color attribute buffer
	//glGenBuffers(1, &player_colorbuffer);
	//glBindBuffer(GL_ARRAY_BUFFER, player_colorbuffer);
	//glBufferData(GL_ARRAY_BUFFER, sizeof(blue_color_buffer_data), blue_color_buffer_data, GL_STATIC_DRAW);
	//
	//// Loads shaders unto our program to use them
	//player_programID = m_shader.LoadShaderFromFile("Assets/vertexshader.txt", "Assets/fragmentshader.txt");
	//
	//// Get a handle for our "MVP" uniform but Only during the initialisation
	//player_matrixID = glGetUniformLocation(player_programID, "MVP");
	//
	//// Spawn 3D objects: Player Cube
	//glBufferData(GL_ARRAY_BUFFER, sizeof(blue_color_buffer_data), blue_color_buffer_data, GL_STATIC_DRAW);
	//
	//glUniformMatrix4fv(player_matrixID, 1, GL_FALSE, &playerMvp[0][0]);
	//glDrawArrays(GL_TRIANGLES, 0, 12 * 3);		// Starting from vertex 0; 12 vertices total -> 1 cube

	return 0;
}

void Player::MovePlayer(vec3 direction)	// move player when key pressed, ... make code
{

	// 2:nd try Bro
	originPos += direction;

	currentPos = originPos;

	//// set new pos for player
	//playerTranslationMatrix = glm::translate(direction);
	//playerMvp = player_projection * player_view * playerTranslationMatrix;
	//// Draw player at new pos
	//glUniformMatrix4fv(player_matrixID, 1, GL_FALSE, &playerMvp[0][0]);
	//glDrawArrays(GL_TRIANGLES, 0, 12 * 3);		// Starting from vertex 0; 12 vertices total -> 1 cube

	//playerTranslationMatrix = translate(currentPos + vec3(1,0,0));
	//playerMvp = newProjection * newView * playerTranslationMatrix;
	//
	//vec3 newTempPos = (currentPos + vec3(1, 0, 0));

	//// color attribute buffer
	//glGenBuffers(1, &player_colorbuffer);
	//glBindBuffer(GL_ARRAY_BUFFER, player_colorbuffer);
	//glBufferData(GL_ARRAY_BUFFER, sizeof(blue_color_buffer_data), blue_color_buffer_data, GL_STATIC_DRAW);

	//// color buffer config
	//glEnableVertexAttribArray(1);
	//glBindBuffer(GL_ARRAY_BUFFER, player_colorbuffer);
	//glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	//// send in initial values
	//glUniformMatrix4fv(player_matrixID, 1, GL_FALSE, &playerMvp[0][0]);
	//// draw call for objects, specify how many triangles to be drawn
	//glDrawArrays(GL_TRIANGLES, 0, 12 * 3);		// Starting from vertex 0; 12 vertices total -> 1 cube

	//std::cout << "move from x: " << currentPos.x << " y: " << currentPos.y << " z: " << currentPos.z << " to: " << newTempPos.x <<   std::endl;
	//currentPos = newTempPos;
}

vec3 Player::GetPlayerPos()
{
	return currentPos;
}

void Player::SetPlayerPos(float x, float y, float z)
{

	originPos = vec3(x, y, z);

	player_projection = player_projection;
	player_view = player_view;
	//std::cout << "new org pos =  " << originPos.x << "." << originPos.y << "." << originPos.z << "." << std::endl;

	playerTranslationMatrix = translate(originPos);
	playerMvp = player_projection * player_view * playerTranslationMatrix;

	// send in initial values
	glUniformMatrix4fv(player_matrixID, 1, GL_FALSE, &playerMvp[0][0]);
	// draw call for objects, specify how many triangles to be drawn
	glDrawArrays(GL_TRIANGLES, 0, 12 * 3);		// Starting from vertex 0; 12 vertices total -> 1 cube
}