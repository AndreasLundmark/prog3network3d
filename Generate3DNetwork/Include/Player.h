#pragma once

#ifndef PLAYER_H
#define PLAYER_H

#include "LibHandler.h"
#include "LoadShader.h"
#include "GenerateWorld.h"

class Player
{

public:
	Player();
	~Player();

	int SetUpPlayer(glm::vec3 spawnPos, glm::mat4 projection, glm::mat4 view);

	void MovePlayer(glm::vec3 direction);

	void UpdatePlayer();

	glm::vec3 GetPlayerPos();
	void SetPlayerPos(float x, float y, float z);

private:
	float m_speed;
	glm::vec3 originPos;

	LoadShader m_shader;
	GLuint player_VertexArrayID;
	GLuint player_vertexBuffer;
	GLuint player_colorbuffer;
	GLuint player_programID;
	GLuint player_matrixID;
	glm::mat4 player_projection;
	glm::mat4 player_view;
};

#endif // !PLAYER_H