#pragma once

#ifndef GENERATEWORLD_H
#define GENERATEWORLD_H

#include "LibHandler.h"
#include "LoadShader.h"
#include "LoadTexture.h"
#include "Player.h"
#include "Client.h"

class GenerateWorld
{

public:
	GenerateWorld();
	~GenerateWorld();

	int SetUpAndCreateWorld();

	void GenerateXAmountOfCubes(int amount, glm::vec3 startpos, float pacing);

	void UpdateWorld();

	glm::mat4 ReturnProjection();
	glm::mat4 ReturnView();

	glm::vec3 orgStartPos;
	glm::mat4 myTranslationMatrix4;
	glm::mat4 loopMvp;

	void NewPlayer1pos(float x, float y, float z);
	void NewPlayer2pos(float x, float y, float z);

private:
	int m_width = 1280;
	int m_height = 960;

	int hejGameWorld = 0;
	
	LoadTexture m_loadtexture;
	LoadShader m_shader;
	GLFWwindow* window;
	GLuint VertexArrayID;
	GLuint vertexBuffer;
	GLuint planeVertexBuffer;
	GLuint colorbuffer;
	GLuint programID;
	GLuint matrixID;
};

#endif // !GENERATEWORLD_H
