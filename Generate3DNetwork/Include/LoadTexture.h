#pragma once

#ifndef LOADTEXTURE_H
#define LOADTEXTURE_H
#define _CRT_SECURE_NO_WARNINGS

#include "LibHandler.h"

class LoadTexture
{

public:
	LoadTexture();
	~LoadTexture();

	GLuint LoadBMPTexture(const char* filename);
	GLuint LoadPNGTexture(const char* filename);

private:

};


#endif // !LOADTEXTURE_H
