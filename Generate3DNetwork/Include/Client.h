//Client.h
#pragma once

#ifndef CLIENT_H
#define CLIENT_H

//#include "stdafx.h"
#include <string>

enum Packet		// create a enum to different packets
{
	P_Unknown, P_Connect, P_PlayerInput, P_GameState,
};

class Client
{

public:
	Client();
	~Client();

	static void ReceiveClientThread();
	bool ClientProcessIncomingPacket(Packet packetid);
	bool SendClientData(Packet packId,int direction);
	int ConnectToGame(std::string gameIP);
	void ConnectClientToServer(std::string IP);

	bool SendPacketID(Packet _packetid);
	static bool GetPacketID(Packet &_packetid);

	void WinsockStartup();
	
	void SetPlayerID(int id);
	int GetPlayerID();

	void GivePos(float x, float y, float z);

	void checkPlayerRacePosition(float x, float y, float z);

private:
	int playerID;
	
};


#endif
