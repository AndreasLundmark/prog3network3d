#pragma once

#ifndef LOADSHADER_H
#define LOADSHADER_H

#include "LibHandler.h"

class LoadShader
{

public:
	LoadShader();
	~LoadShader();

	GLuint LoadShaderFromFile(const char* vertex_file_path, const char* fragment_file_path);

private:

};

#endif // !LOADSHADER_H
