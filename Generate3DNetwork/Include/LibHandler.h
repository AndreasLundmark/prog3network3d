#pragma once

#ifndef LIBHANDLER_H
#define LIBHANDLER_H

#include <Windows.h>
// openGL usage libs
#include <GL/glew.h>
#include <GL/GL.h>
#include <GLFW/glfw3.h>

// openGL math libs
#include "glm\glm.hpp"
#include "glm\gtx\transform.hpp"
#include "glm\gtc\matrix_transform.hpp"

// standard VS libs
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <fstream>
#include <vector>
#endif // !LIBHANDLER
